#include <SPI.h> // SPI-Bibiothek hinzufügen
#include <MFRC522.h> // RFID-Bibiothek hinzufügen

#define SS_PIN 53 // SDA an Pin 10 (bei MEGA anders)
#define RST_PIN 5 // RST an Pin 9 (bei MEGA anders)

#define TL_RED_PIN 45
#define TL_YELLOW_PIN 43
#define TL_GREEN_PIN 44

#define TL_RED 2
#define TL_YELLOW 1
#define TL_GREEN 0

MFRC522 mfrc522(SS_PIN, RST_PIN); // RFID-Empfänger benennen

void setTrafficLight(int select) {
  if (select == 0) {
    digitalWrite (TL_RED_PIN, LOW);
    digitalWrite (TL_YELLOW_PIN, LOW);
    digitalWrite (TL_GREEN_PIN, HIGH);    
  } 
  else if (select == 1) {
    digitalWrite (TL_RED_PIN, LOW);
    digitalWrite (TL_YELLOW_PIN, HIGH);
    digitalWrite (TL_GREEN_PIN, LOW);    
  } 
  else {
    digitalWrite (TL_RED_PIN, HIGH);
    digitalWrite (TL_YELLOW_PIN, LOW);
    digitalWrite (TL_GREEN_PIN, LOW);    
  }
}

void setup() // Beginn des Setups:
{
  Serial.begin(9600); // Serielle Verbindung starten (Monitor)
  while (!Serial); // wareten 

  SPI.begin(); // SPI-Verbindung aufbauen
  mfrc522.PCD_Init(); // Initialisierung des RFID-Empfängers

  delay(5); // 
  mfrc522.PCD_DumpVersionToSerial();
  
  setTrafficLight(TL_YELLOW);
  
  Serial.println("Setup fertig!");
}

void loop() // Hier beginnt der Loop-Teil
{
  if ( ! mfrc522.PICC_IsNewCardPresent()) // Wenn keine Karte in Reichweite ist...
  {
    //Serial.println("keine Karte zu finden");
    return; // ...springt das Programm zurück vor die if-Schleife, womit sich die Abfrage wiederholt.
  } else {
    Serial.println("neue Karte gefunden");
  }

  if ( ! mfrc522.PICC_ReadCardSerial()) // Wenn kein RFID-Sender ausgewählt wurde
  {
    //Serial.println("kein RFID Leser zu finden");
    return; // ...springt das Programm zurück vor die if-Schleife, womit sich die Abfrage wiederholt.
  } else {
    Serial.println("RFID Leser gefunden");
  }
    
  // Serial.print("Die ID des RFID-TAGS lautet:"); // "Die ID des RFID-TAGS lautet:" wird auf den Serial Monitor geschrieben.

  // for (byte i = 0; i < mfrc522.uid.size; i++)
  // {
  //   Serial.print(mfrc522.uid.uidByte[i], HEX); // Dann wird die UID ausgelesen, die aus vier einzelnen Blöcken besteht und der Reihe nach an den Serial Monitor gesendet. Die Endung Hex bedeutet, dass die vier Blöcke der UID als HEX-Zahl (also auch mit Buchstaben) ausgegeben wird
  //   Serial.print(" "); // Der Befehl „Serial.print(" ");“ sorgt dafür, dass zwischen den einzelnen ausgelesenen Blöcken ein Leerzeichen steht.
  // }

  // Serial.println(); // Mit dieser Zeile wird auf dem Serial Monitor nur ein Zeilenumbruch gemacht.

  long code=0; // Als neue Variable fügen wir „code“ hinzu, unter welcher später die UID als zusammenhängende Zahl ausgegeben wird. Statt int benutzen wir jetzt den Zahlenbereich „long“, weil sich dann eine größere Zahl speichern lässt.

  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    code=((code+mfrc522.uid.uidByte[i])*10); // Nun werden wie auch vorher die vier Blöcke ausgelesen und in jedem Durchlauf wird der Code mit dem Faktor 10 „gestreckt“. (Eigentlich müsste man hier den Wert 1000 verwenden, jedoch würde die Zahl dann zu groß werden.
  }

  Serial.print("Die Kartennummer lautet:"); // Zum Schluss wird der Zahlencode (Man kann ihn nicht mehr als UID bezeichnen) ausgegeben.
  Serial.println(code);

  // Ab hier erfolgt die erweiterung des Programms.

  if (code==881730) // Wenn der Zahlencode 1031720 lautet...
  { // Programmabschniss öffnen
    setTrafficLight(TL_GREEN);
    Serial.println("Willkommen Freund!!!");
  }
  else {
    setTrafficLight(TL_RED);
    Serial.println("Geh weg Fremder!!!");
  }

   // … und danach wieder aus gehen.
  delay (3000); // für 5 Sekunden
  setTrafficLight(TL_YELLOW);
}